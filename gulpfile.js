const gulp = require('gulp');
const cache = require('gulp-cache');
const runSequence = require('run-sequence');
const browserSync = require('browser-sync').create(); /* https://browsersync.io/docs/gulp */


// path config
const entryJSPoint = './src/js/App.js';
const browserDir = './';
const sassWatchPath = './src/sass/**/*.scss';
const jsWatchPath = './src/js/**/*.js';
const imgWatchPath = './src/sprite_img/**/*.png';
const htmlWatchPath = './html/**/*.html';


gulp.task('bs', function() {
	browserSync.init({
		server: {
			baseDir: browserDir,
			directory: true,
		},
		port: 2020,
		ui: {
			port: 58080,
			weinre: {
				port: 59090
			}
		},
		open: false
	});
});

gulp.task('init', function() {
	runSequence('bs');
});

gulp.task('watch', function() {
	watch(htmlWatchPath, function() {
		return gulp.src('')
			.pipe(browserSync.reload({ stream: true }))
	});
});

gulp.task('clean', function(done) {
	return cache.clearAll(done);
});

// gulp develop
gulp.task('default', ['clean', 'init', 'watch']);
// gulp.task('default', gulp.series('clean', 'init','watch', function(done){
// 	done();
// }));