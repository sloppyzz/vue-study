import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import testPage from '@/components/testPage'

// import testHeader from '@/components/route/headerTest'
// import testBody from '@/components/route/bodyTest'
// import testFooter from '@/components/route/footerTest'

Vue.use(Router)

export default new Router({
    mode: 'history',
    base: __dirname,
    routes: [{
            path: '/',
            name: 'HelloWorld',
            component: HelloWorld,
            meta: { isMain: true }
        },
        {
            path: '/test',
            name: 'testPage',
            component: testPage,
            meta: { isMain: false }
            // children: {
            //     header: testHeader,
            //     body: testBody,
            //     footer: testFooter
            // }
        }
    ]
})
