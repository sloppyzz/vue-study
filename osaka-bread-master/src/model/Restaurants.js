import firebase from 'firebase'
import { db } from '@/config/firebaseInit'

class Restaurants {
  constructor () {
    this.collection_name = 'restaurants';
    this.docRef = null;
  }

  saveRestaurant ( data ) {
    data.create_time = new Date().getTime();
    return new Promise(((resolve, reject) => {
      let docRef = db.collection( this.collection_name ).doc();
      docRef.set( data )
        .then(()=>{
          resolve( docRef );
          // console.log( result );
          console.log(this.collection_name + ' Document Successfully Written!');
        })
        .catch(error=>{
          reject(error);
          throw error;
        })
    }));
  }

  editRestaurant ( doc, data){
    data.create_time = new Date().getTime();
    return new Promise((resolve, reject)=> {
      let docRef = db.collection( this.collection_name ).doc( doc );
      docRef.update( data ).then(()=>{
        resolve( docRef );
        console.log(this.collection_name + ' Document Successfully Update!');
      })
        .catch(error=>{
          reject(error);
          throw error;
        })
    })
  }

  removeResTag ( pk, tagField, tagName){
    let resRef = db.collection( this.collection_name ).doc(pk);
    return resRef.get().then(result=>{
      return new Promise(resolve=>{
        resRef.update({
          [tagField+'.'+tagName] : firebase.firestore.FieldValue.delete()
        });
        resolve();
        console.log([tagField+'.'+tagName] +' 태그 삭제 성공');
      })
    })
  }


  deleteRestaurant ( pk ) {
    let ref = db.collection( this.collection_name ).doc(pk);

    return new Promise(resolve=>{
      ref.delete().then(()=>{
        console.log(pk + ' res document delete success')
        resolve()
      }).catch(error=>{
        console.log( error )
      });
    })
  }
}

export default Restaurants;
