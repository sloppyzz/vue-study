import firebase from 'firebase'
import { db } from '@/config/firebaseInit'

class User {
  constructor(){
    this.collection_name ='users';

  }


  isLikeRes ( uid ) {
    return new Promise((resolve => {
      let docRef = db.collection( this.collection_name ).doc(uid);
      docRef.get().then(result=>{
        resolve(result.data());
      })
    }))
  }

  getLikeRes ( uid ){
    let docRef = db.collection( this.collection_name ).doc(uid);

    return new Promise((resolve)=>{
      docRef.get().then(result=>{
        resolve( result.data().like_res )
      })
    })
  }

  addLikeRes ( uid, resPk ){
    let docRef = db.collection( this.collection_name ).doc(uid);

    let oResPk = {};
    oResPk[resPk] = true;

    return new Promise(( resolve, reject )=>{
      docRef.set({
        'like_res' : oResPk
      }, { merge : true }).then(()=>{
        resolve('좋아요!!');
      }).catch(error=>{
        console.log( error );
        reject();
      })
    })
  }

  removeLikeRes ( uid, resPk ) {
    return new Promise((resolve, reject) => {
      let docRef = db.collection(this.collection_name).doc(uid);
      docRef.update({
        ['like_res.' + resPk]: firebase.firestore.FieldValue.delete()
      }).then(()=>{
        resolve('좋아요 취소!');
      }).catch(error=>{
        console.log(error);
        reject()
      })
    })

  }
}

export default User;
