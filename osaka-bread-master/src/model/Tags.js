import firebase from 'firebase'
import { db } from '@/config/firebaseInit'

class Tags {
  constructor () {
    this.collection_name = 'tags';
  }


  saveKindTag ( data, refRes ) {
    let dbeRef = db.collection(this.collection_name).doc( 'tags_kind' );

    return new Promise(((resolve, reject) => {
      dbeRef.get().then((result)=>{

        Object.keys( data ).map( (key, index)=> {

          let tag = null;

          if ( result.data() && result.data()[key] ){
            tag = result.data()[key];
          }

          if( tag ){
            // tag.push( refRes );
            dbeRef.set({
              [key] : { [refRes.id]: true}
            },{ merge : true })
          } else{
            dbeRef.set({
              [key] : { [refRes.id] : true }
            },{ merge : true })
          }

        });

        resolve('종류테그 쓰기 완료');

      });

    }));

  }
  saveCityTag ( data, refRes ) {
    let dbRef = db.collection(this.collection_name).doc( 'tags_city' );

    return new Promise(((resolve, reject) => {
      dbRef.get().then((result)=>{

        Object.keys( data ).map( (key, index)=> {

          let tag = null;

          if ( result.data() && result.data()[key] ){
            tag = result.data()[key];
          }

          if( tag ){

            dbRef.set({
              [key] : { [refRes.id]: true}
            },{ merge : true })
          } else{
            dbRef.set({
              [key] : { [refRes.id]: true}
            },{ merge : true })
          }

        });

        resolve(' 시티 테그 쓰기 완료');

      });

    }));
  }

  deleteTag ( oTagName, doc, pk ){
    let ref = db.collection( this.collection_name );
    let docRef = ref.doc(doc);

    return Promise.all(
      Object.keys( oTagName ).map((key, index)=>{
        return new Promise(resolve=>{
          console.log(key);
          docRef.update({
            [key+'.'+pk] : firebase.firestore.FieldValue.delete()
          });
          resolve();
          console.log([key+'.'+pk] +' 태그 삭제 성공')
        })
      })
    )

  }

  deleteEmptyTag ( doc ){
    let ref = db.collection( this.collection_name );
    let docRef = ref.doc(doc);
    let promises = [];

    return docRef.get().then(doc=>{

      return Promise.all(Object.keys(doc.data()).map((key, index)=>{
        return new Promise(resolve=>{
          if( Object.keys( doc.data()[key] ).length === 0 && doc.data()[key].constructor === Object ){
            docRef.update({
              [key] : firebase.firestore.FieldValue.delete()
            })
            console.log(key + ' 삭제 성공')
          }
          resolve()

        })
      }))
    })
  }
}

export default Tags;

