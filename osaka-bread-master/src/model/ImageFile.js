import firebase from 'firebase'
import { EventBus } from '@/store/eventBus'

class ImageFile {
  constructor () {
    this.image_list = [];

  }

  static _makeS4 () {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  }

  static _makeFileUUID () {
    return ImageFile._makeS4() + ImageFile._makeS4() + '-' + ImageFile._makeS4() + '-' + ImageFile._makeS4() + '-' + ImageFile._makeS4() + '-' + ImageFile._makeS4() + ImageFile._makeS4() + ImageFile._makeS4();
  }

  static _splitImageType (sImageType ) {
    return sImageType.split('/')[1];
  }

  _imageUpload ( image, index ) {
    let storageRef = firebase.storage().ref();
    let uuidFileName = ImageFile._makeFileUUID()+'.'+ImageFile._splitImageType( image.type );
    let relativePath = 'osakaBread/'+ uuidFileName;

    return new Promise((resolve, reject) => {

      let uploadTask = storageRef.child(relativePath).put(image);

      uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
        function startUpload(snapshot) {
          let progress = ( snapshot.bytesTransferred / snapshot.totalBytes ).toFixed(1) * 100;
          switch (snapshot.state) {
            case firebase.storage.TaskState.RUNNING:
              EventBus.$emit("UploadImage", index, progress);
              break;
          }

        },
        function errorUpload(error) {
          reject();
          console.log(error);
          uploadTask.cancel();
          alert(error.code);
        },

        function successUpload() {
          let abs_path = uploadTask.snapshot.downloadURL;
          EventBus.$emit("CompleteImageUpload", abs_path, relativePath, index);
          resolve( abs_path );
        }
      )
    });
  }

  setImageList ( images ) {
    this.image_list = images;
    console.log( this.image_list );
  }

  saveImageFile () {
    EventBus.$emit("UploadStart", true);

    return Promise.all( this.image_list.map(( obj, index)=>{
      return this._imageUpload( obj.file, index );
    }) );
  }

  deleteImageFile ( aImage ) {
    return Promise.all( aImage.map((obj, index)=>{
      return new Promise((resolve => {
        let storage = firebase.storage();
        let storageRef = storage.ref();
        let desertRef = storageRef.child(obj.rel_path);

        desertRef.delete().then(()=>{
          resolve();
          console.log(obj.rel_path + ' delete success!');
        }).catch(error=>{
          console.log( error );
        })
      }))
    }))
  }

}

export default ImageFile;
