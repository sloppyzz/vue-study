import firebase from 'firebase'
import { db } from '@/config/firebaseInit'

export const adminConfirm = {
  data () {
    return {
      isAdmin : false
    }
  },
  beforeCreate () {
    let uid = null;
    firebase.auth().onAuthStateChanged(user => {
      if(user == null) {
        this.isAdmin = false;
        this.$router.push("/")
      } else {
        uid = db.collection("users").doc( user.uid ).get().then(result=>{
          // console.log( result.data() );
          if( result.data().admin !== true ){
            this.$router.push("/");
          } else {
            this.isAdmin = true;
          }
        });
      }
    })
  },
}
