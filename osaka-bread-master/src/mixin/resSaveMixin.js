import { db } from '@/config/firebaseInit'

import Restaurant from '@/model/Restaurants'
import Tags from '@/model/Tags'

export default {
  data(){
    return {
      formData : {
        city : '',
        name : '',
        foreign_name: '',
        tags_kind : {},
        tags_city : {},
        rating : '평점 선택',
        address : '',
        map_url : '',
        description : '',
        list_image : [{
          origin_url : '',
          convert_url : ''
        }],
        create_time : 0,
      },


      // preview images
      preview_images : [],
      // form Select Option bind Item
      cities : [
        { text: '서울', value : '서울'},
        { text: '도쿄', value : '도쿄'},
        { text: '오사카', value : '오사카'},
        { text: '교토', value : '교토'},
      ],

      //upload state
      isUploading : false,

    }
  },

  watch : {
    'formData.tags_city' : function(newVal){
      console.log(newVal);
    }
  },

  beforeCreate(){
    if(this.$route.params.res_pk){
      let resRef = db.collection('restaurants').doc(this.$route.params.res_pk);
      resRef.get().then(result=>{
        // 식당 정보 존재 하는지 확인
        let fData = null;
        if(result.exists){
          fData = result.data();
          console.log( fData );
          this.formData = fData;
          // this.setTagResult( fData.tags_kind, fData.tags_city );
        } else {
          this.$router.push('/admin');
        }
      })
    }

  },

  methods : {

    isEmpty(obj) {
      for(var key in obj) {
        if(obj.hasOwnProperty(key))
          return false;
      }
      return true;
    },

    setTagResult ( tKind, tCity ) {
      this.register_tag.kind = tKind;
      this.register_tag.city = tCity;

      console.log (this.register_tag);
    },

    addImageRow(){
      let obj = {
        origin_url : '',
        convert_url : ''
      } ;
      this.formData.list_image.push( obj );
    },

    resetImageRow (){
      this.formData.list_image = [{
        origin_url : '',
        convert_url : ''
      }]
    },

    removeRow (index){
      this.formData.list_image.splice(index, 1);
    },

    convertURL (){

      this.formData.list_image.map((obj, index)=>{

        if (this.isValidUrl( obj.origin_url )) {
          alert(index +" 경로가 잘못 되었습니다.");
          obj.origin_url = '';
          return false;
        }

        let gdId = this.getParameterByName("id", obj.origin_url);
        let prefix = "http://drive.google.com/uc?export=view&id=";

        obj.convert_url = prefix + gdId;

      });

    },

    isValidUrl(url) {
      let parser = document.createElement('a');
      parser.href = url;
      return (url === '' || parser.hostname !== "drive.google.com" || !parser.search.includes("?id="));

    },

    getParameterByName(name, url) {
      if (!url) url = this.$route.fullPath;
      name = name.replace(/[\[\]]/g, "\\$&");
      let regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
      if (!results) return null;
      if (!results[2]) return '';
      return decodeURIComponent(results[2].replace(/\+/g, " "));
    },

    checkImageList () {
      return this.formData.list_image.every(item=>{
        return this.formData.list_image.length === 0 || item.convert_url === '' ;
      })
    },

    validateFormData () {
      let fd = this.formData;
      let url_regex = /^((http|https):\/\/)/;

      if( fd.name.length === 0 ) {
        throw "식당 이름을 입력해 주세요."
      } else if( this.checkImageList() ){
        throw "사진 경로의 URL이 비어있습니다."
      } else if( fd.city.length === 0 ) {
        throw "식당이 위치한 도시를 선택해 주세요."
      } else if ( fd.address.length === 0 ){
        throw "식당 주소를 입력해 주세요."
      } else if ( !url_regex.test( fd.map_url) ){
        throw "지도 URL이 아닙니다. URL을 확인해 주세요."
      } else if ( fd.description.length === 0 ) {
        throw "식당 설명을 입력해 주세요."
      } else if ( fd.tags_kind.length === 0 ) {
        throw "식당과 관련된 태그를 입력해 주세요."
      } else if ( fd.tags_city.length === 0) {
        throw "식당이 위치한 도시 태그를 입력해 주세요."
      } else if ( fd.rating.length === 0) {
        throw "평점을 선택해 주세요."
      } else {
        return true;
      }
    },

    setCityHashTag ( event ) {
      event.target.value.replace(/#[^#\s,;]+/gm, (tag)=> {
        let convert_tag = tag.split('#')[1];
        this.formData.tags_city[convert_tag] = true;
      });

    },

    setKindHashTag ( event ) {
      event.target.value.replace(/#[^#\s,;]+/gm, (tag) => {
        let convert_tag = tag.split('#')[1];
        this.formData.tags_kind[convert_tag] = true;
      });
    },

    saveResInfo ( pk ) {
      let promises = null;
      let restaurant = new Restaurant();
      let tags = new Tags();

      try {
        // 벨리데이션
        if( this.validateFormData() ){
          this.isUploading = true;
          if( pk ) {
            promises = restaurant.editRestaurant( pk, this.formData );
          } else {
            promises = restaurant.saveRestaurant( this.formData );
          }


          promises.then((docRef)=>{
            let apro = [];
            apro.push( tags.saveKindTag( this.formData.tags_kind, docRef ));
            apro.push( tags.saveCityTag( this.formData.tags_city, docRef ));

            return Promise.all(apro);
          })
            .then(()=>{
              console.log("업로드 성공");
              this.$router.push('/admin');
            })
            .catch((error)=>{
              this.isUploading = false;
              console.log( error );
              alert(" 업로드 실패 ");
            });
        }
      } catch (error) {
        this.isUploading = false;
        console.log( error );
        alert( error );
      }
    },

  }
}
