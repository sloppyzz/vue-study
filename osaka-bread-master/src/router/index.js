import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/pages/Main'
import Detail from '@/pages/Detail'
import CityList from '@/pages/CityList'

import Admin_Main from '@/pages/admin/Main'
import Admin_Add from '@/pages/admin/Add'
import Admin_Edit from '@/pages/admin/Edit'

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main
    },
    {
      path: '/list/:city',
      name: 'CityList',
      component: CityList
    },
    {
      path: '/detail/:res_pk',
      // query : { res_pk : ''},
      name: 'Detail',
      component: Detail
    },

    // admin
    {
      path: '/admin/',
      name:'Admin_Main',
      component : Admin_Main
    },
    {
      path: '/admin/add',
      name: 'Admin_Add',
      component: Admin_Add
    },
    {
      path: '/admin/edit/:res_pk',
      name: 'Admin_Edit',
      component: Admin_Edit
    },

    {
      path:'*',
      component: Main
    }

  ]
})
