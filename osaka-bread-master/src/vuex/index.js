import Vue from 'vue'
import Vuex from 'vuex'
import loginCheck from './modules/loginCheck'


Vue.use(Vuex)

export default new Vuex.Store({
  modules :{
    loginCheck
  }

})
