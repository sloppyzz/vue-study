const state = {
  isLogin : false
};

const mutations = {
  SET_LOGIN (state) {
    state.isLogin = true
  },

  SET_LOGOUT (state) {
    state.isLogin = false
  }
}

const actions = {
  setLogin({commit}){
    commit('SET_LOGIN')
  },

  setLogout({commit}){
    commit('SET_LOGOUT')
  }
}

const getters = {
  getLoginState (state) {
    return state.isLogin
  }
}

export default {
  state,
  mutations,
  getters,
  actions
}
