# 나중에 좀더 찾아보자

- https://github.com/vuejs/awesome-vue
- http://tech.javacafe.io/2017/11/14/Vuejs-%EA%B2%BD%ED%97%98-%ED%9B%84%EA%B8%B0-%EA%B3%B5%EC%9C%A0/

----

### resource 관련 
- https://kr.vuejs.org/v2/guide/reactivity.html#Declaring-Reactive-Properties
- https://forum.vuejs.org/t/list-json-objects-from-url/4122/2
- https://www.google.co.kr/search?q=vuejs+get+json+list+render+list&rlz=1C1CHZL_koKR750KR750&oq=vuejs+get+json+list+render+list&aqs=chrome..69i57.16889j0j7&sourceid=chrome&ie=UTF-8

-----

### vue sass
- https://www.google.co.kr/search?q=vue+sass+scoped&oq=vue+sass+scoped&aqs=chrome..69i57j0l2.8451j0j7&sourceid=chrome&ie=UTF-8
- https://vueschool.io/articles/vuejs-tutorials/globally-load-sass-into-your-vue-js-applications/
- https://stackoverflow.com/questions/48490969/webpack-simple-vue-cli-with-sass-cant-compile?rq=1

### 고급

	# vuex
	- https://vuex.vuejs.org/kr/guide/
	- 어플리케이션의 상태 관리 라이브러리/ (상/하) 컴포넌트간의 데이터 통신
	
	# 반응형(vue reactivity)
	- https://kr.vuejs.org/v2/guide/reactivity.html
	- 데이터 변화를 감지해 자동으로 화면 갱신
	- instance data, getter, setter, watcher
	- data 인스턴스에 반응성이 생기는 시점은 인스턴스를 생성하는 시점.

	# 서버 사이드 렌더링
	- https://kr.vuejs.org/v2/guide/ssr.html
	- 서버사이드 렌더링 / 클라이언트 사이드 렌더링
	- 서버사이드 렌더링
		- 검색엔진 최적화의 강점
		- 초기 화면 렌더링 속도가 빠름
	- 클라이언트 사이드 렌더링
		- 매끄러운 화면 전환과 사용자 경험 향상

	# vue with jquery 
	- http://vuejs.kr/jekyll/update/2017/03/02/vuejs-jquery-bootstrap/
	- https://vuejsdevelopers.com/2017/05/20/vue-js-safely-jquery-plugin/