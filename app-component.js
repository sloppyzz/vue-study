Vue.component('component-global', {
    template: '<p>this is another global child component</p>'
});

var locom = {
    template: '<p>this is another child component</p>'
}

var app = new Vue({
    el: '#app',
    data: {
        message: 'this is parent component'
    },
    components: {
        'component-local': locom
    }
});